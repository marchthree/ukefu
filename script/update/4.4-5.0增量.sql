CREATE TABLE `uk_pbxhost_orgi_rela` (
    `id` varchar(32) NOT NULL COMMENT '主键ID',
    `pbxhostid` varchar(32) DEFAULT NULL COMMENT '联系人id',
    `orgi` varchar(200) DEFAULT NULL COMMENT '关联的租户id',
    `creater` varchar(50) DEFAULT NULL COMMENT '创建人',
    `updater` varchar(50) DEFAULT NULL COMMENT '更新人',
    `createtime` datetime DEFAULT NULL COMMENT '创建时间',
    `updatetime` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT COMMENT='语音平台 和 租户 绑定';

ALTER TABLE uk_user ADD tenant tinyint(4) DEFAULT 0 COMMENT '是否是新版多租户中新增的用户（true 是）';

ALTER TABLE uk_tenant ADD agentnum INT(11) DEFAULT 0 COMMENT '文本坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_tenant ADD callcenteragentnum INT(11) DEFAULT 0 COMMENT '呼叫中心坐席数量 （云平台坐席限制使用）';
ALTER TABLE uk_tenant ADD models VARCHAR(500) DEFAULT NULL COMMENT '授权模块';

ALTER TABLE uk_tenant ADD datastatus tinyint(4) DEFAULT '0' COMMENT '数据状态（0 未删除| 1 已删除到回收站）';

ALTER TABLE uk_callcenter_pbxhost ADD defaultpbx tinyint(4) DEFAULT '0' COMMENT '是否默认 分配的语音服务器';


ALTER TABLE uk_tenant ADD robotagentnum INT(11) DEFAULT 0 COMMENT '电销机器人数量 （云平台坐席限制使用）';

ALTER TABLE uk_systemconfig ADD robotagentnum INT(11) DEFAULT 0 COMMENT '电销机器人数量 （云平台坐席限制使用）';
ALTER TABLE uk_systemconfig ADD enablecloud tinyint(4) DEFAULT 0 COMMENT '是否云平台服务';


ALTER TABLE uk_blacklist ADD listtype varchar(50) DEFAULT NULL COMMENT '名单类型(blacklist、黑名单  redlist、红名单  viplist、vip名单)';
ALTER TABLE uk_blacklist ADD extentions varchar(2000) DEFAULT NULL COMMENT '分机号';
ALTER TABLE uk_blacklist ADD skillname varchar(32) DEFAULT NULL COMMENT '技能组名称';

ALTER TABLE uk_callcenter_extention ADD assigned tinyint(4) DEFAULT 0 COMMENT '是否分配 多租户使用';