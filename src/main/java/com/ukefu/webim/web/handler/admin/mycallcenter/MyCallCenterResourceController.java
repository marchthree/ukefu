package com.ukefu.webim.web.handler.admin.mycallcenter;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ukefu.util.Menu;
import com.ukefu.webim.service.repository.ExtentionRepository;
import com.ukefu.webim.service.repository.MediaRepository;
import com.ukefu.webim.service.repository.PbxHostRepository;
import com.ukefu.webim.service.repository.ServiceAiRepository;
import com.ukefu.webim.web.handler.Handler;
import com.ukefu.webim.web.model.PbxHost;

@Controller
@RequestMapping("/admin/mycallcenter")
public class MyCallCenterResourceController extends Handler{
	
	@Autowired
	private PbxHostRepository pbxHostRes ;
	
	@Autowired
	private ExtentionRepository extentionRes;
	
	
	@RequestMapping(value = "/resource")
    @Menu(type = "callcenter" , subtype = "mycallcenter" , access = false )
    public ModelAndView index(ModelMap map , HttpServletRequest request , @Valid String hostid) {
		PbxHost pbxHost = pbxHostRes.findById(hostid) ;
		//map.addAttribute("pbxHostList" , pbxHostList);
		if(pbxHost != null){
			map.addAttribute("pbxHost" , pbxHost);
			if(super.isEnabletneantAndSuperUser(request)){
				//启用多租户和超级管理员 显示全部
				map.addAttribute("extentionList" , extentionRes.findByHostid(pbxHost.getId()));
			}else{
				map.addAttribute("extentionList" , extentionRes.findByHostidAndOrgi(pbxHost.getId() , super.getOrgi(request)));
			}
		}
		map.addAttribute("ismy" , true);
		return request(super.createAdminTempletResponse("/admin/mycallcenter/resource/index"));
    }
	
}
