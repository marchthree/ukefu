package com.ukefu.webim.web.model;

import java.io.Serializable;

public class ContactsItemTargetdis implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -594962903970778219L;
	private String targetid;
	private int distimes;
	public String getTargetid() {
		return targetid;
	}
	public void setTargetid(String targetid) {
		this.targetid = targetid;
	}
	public int getDistimes() {
		return distimes;
	}
	public void setDistimes(int distimes) {
		this.distimes = distimes;
	}
	

}
