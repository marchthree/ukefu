package com.ukefu.webim.web.model;

import java.io.Serializable;

public class DisModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8146810678920578657L;

	public DisModel(String dataid , String type) {
		this.dataid = dataid ;
		this.type = type ;
	}
	private String dataid ;
	private String type ;
	public String getDataid() {
		return dataid;
	}
	public void setDataid(String dataid) {
		this.dataid = dataid;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	
}
