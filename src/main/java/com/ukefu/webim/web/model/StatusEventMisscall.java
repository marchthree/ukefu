package com.ukefu.webim.web.model;

import com.ukefu.util.event.UserEvent;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Proxy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "uk_callcenter_event")
@Proxy(lazy = false)
public class StatusEventMisscall implements Serializable, UserEvent {

	private static final long serialVersionUID = -1475362358232114392L;

	private String id ;

	private boolean misscall = true;	//是否漏话
	

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "assigned")
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public boolean isMisscall() {
		return misscall;
	}

	public void setMisscall(boolean misscall) {
		this.misscall = misscall;
	}

	@Override
	public String toString() {
		return "StatusEventMisscall{" +
				"id='" + id + '\'' +
				", misscall=" + misscall +
				'}';
	}
}
