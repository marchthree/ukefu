package com.ukefu.webim.web.model;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * 语音平台 和 租户 绑定
 */
@Entity
@Table(name = "uk_pbxhost_orgi_rela")
@org.hibernate.annotations.Proxy(lazy = false)
public class PbxHostOrgiRela implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2003055823765517877L;
	private String id;
	private String pbxhostid;

	private String orgi ;//绑定的租户id
	private String creater ;
	private String updater ;
	private Date createtime = new Date();
	private Date updatetime = new Date();

	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getPbxhostid() {
		return pbxhostid;
	}

	public void setPbxhostid(String pbxhostid) {
		this.pbxhostid = pbxhostid;
	}

	public String getOrgi() {
		return orgi;
	}

	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getCreatetime() {
		return createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Date getUpdatetime() {
		return updatetime;
	}

	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}

	public String getUpdater() {
		return updater;
	}

	public void setUpdater(String updater) {
		this.updater = updater;
	}
}
