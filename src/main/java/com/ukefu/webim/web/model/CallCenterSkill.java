package com.ukefu.webim.web.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "uk_callcenter_skill")
@org.hibernate.annotations.Proxy(lazy = false)
public class CallCenterSkill implements java.io.Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3932323765657445180L;
	private String id;
	private String name;
	private String skill;	//Or IP
	private String hostid ;
	private String quene;
	private String password ;	//pbx host password
	
	private String orgi;
	private String creater ;
	private Date createtime = new Date();
	private Date updatetime = new Date();
	
	private String siptrunk;//绑定网关
	
	private String strategy ;//策略模式
	private String mohsound ;//语音文件参数
	private String announcesound ;//播报语音
	private int announcefrequency ;//播报频率
	private String recordtemplate ;//录音文件参数
	private String timebasescore ;//时基分数
	private int maxwaittime ;//最大等待时间
	private int maxwtnoagent ;//最大等待时间与无代理
	private int maxwtnareached ;//拒绝新来电间隔时间
	private String trapply ;//等级规则应用 true/false
	private int trwaitsecond ;//等级规则设置的时间
	private String trmultiplylevel ;//等级规则级别 true/false
	private String trnoagentnowait ;//是否跳过等级规则 true/false
	private String abanresumealo ;//重新进入队列 true/false
	private int disabanafter ;//最大放弃时长
	
	@Id
	@Column(length = 32)
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	public String getOrgi() {
		return orgi;
	}
	public void setOrgi(String orgi) {
		this.orgi = orgi;
	}
	public String getCreater() {
		return creater;
	}
	public void setCreater(String creater) {
		this.creater = creater;
	}
	public Date getCreatetime() {
		return createtime;
	}
	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}
	public Date getUpdatetime() {
		return updatetime;
	}
	public void setUpdatetime(Date updatetime) {
		this.updatetime = updatetime;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSkill() {
		return skill;
	}
	public void setSkill(String skill) {
		this.skill = skill;
	}
	public String getQuene() {
		return quene;
	}
	public void setQuene(String quene) {
		this.quene = quene;
	}
	public String getHostid() {
		return hostid;
	}
	public void setHostid(String hostid) {
		this.hostid = hostid;
	}
	public String getSiptrunk() {
		return siptrunk;
	}
	public void setSiptrunk(String siptrunk) {
		this.siptrunk = siptrunk;
	}
	public String getStrategy() {
		return strategy;
	}
	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}
	public String getMohsound() {
		return mohsound;
	}
	public void setMohsound(String mohsound) {
		this.mohsound = mohsound;
	}
	public String getAnnouncesound() {
		return announcesound;
	}
	public void setAnnouncesound(String announcesound) {
		this.announcesound = announcesound;
	}
	public int getAnnouncefrequency() {
		return announcefrequency;
	}
	public void setAnnouncefrequency(int announcefrequency) {
		this.announcefrequency = announcefrequency;
	}
	public String getRecordtemplate() {
		return recordtemplate;
	}
	public void setRecordtemplate(String recordtemplate) {
		this.recordtemplate = recordtemplate;
	}
	public String getTimebasescore() {
		return timebasescore;
	}
	public void setTimebasescore(String timebasescore) {
		this.timebasescore = timebasescore;
	}
	public int getMaxwaittime() {
		return maxwaittime;
	}
	public void setMaxwaittime(int maxwaittime) {
		this.maxwaittime = maxwaittime;
	}
	public int getMaxwtnoagent() {
		return maxwtnoagent;
	}
	public void setMaxwtnoagent(int maxwtnoagent) {
		this.maxwtnoagent = maxwtnoagent;
	}
	public int getMaxwtnareached() {
		return maxwtnareached;
	}
	public void setMaxwtnareached(int maxwtnareached) {
		this.maxwtnareached = maxwtnareached;
	}
	public String getTrapply() {
		return trapply;
	}
	public void setTrapply(String trapply) {
		this.trapply = trapply;
	}
	public int getTrwaitsecond() {
		return trwaitsecond;
	}
	public void setTrwaitsecond(int trwaitsecond) {
		this.trwaitsecond = trwaitsecond;
	}
	public String getTrmultiplylevel() {
		return trmultiplylevel;
	}
	public void setTrmultiplylevel(String trmultiplylevel) {
		this.trmultiplylevel = trmultiplylevel;
	}
	public String getTrnoagentnowait() {
		return trnoagentnowait;
	}
	public void setTrnoagentnowait(String trnoagentnowait) {
		this.trnoagentnowait = trnoagentnowait;
	}
	public String getAbanresumealo() {
		return abanresumealo;
	}
	public void setAbanresumealo(String abanresumealo) {
		this.abanresumealo = abanresumealo;
	}
	public int getDisabanafter() {
		return disabanafter;
	}
	public void setDisabanafter(int disabanafter) {
		this.disabanafter = disabanafter;
	}
	
}
