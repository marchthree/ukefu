package com.ukefu.webim.service.es;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.ukefu.webim.web.model.MRoundAnswer;

public interface MRoundAnswerRepository
extends  ElasticsearchRepository<MRoundAnswer, String>
{
	public abstract List<MRoundAnswer> findByDataidAndOrgi(String dataid , String orgi);
	public abstract MRoundAnswer findById(String id);
}
