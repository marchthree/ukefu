package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.ukefu.webim.web.model.NumberPool;

public interface NumberPoolRepository extends JpaRepository<NumberPool, String> {

    public NumberPool findById(String id) ;

    public Page<NumberPool> findByHostid(String hostid, Pageable page) ;

    public List<NumberPool> findByHostid(String hostid) ;

    public long countByNumberAndHostid(String number,String hostid) ;

    public long countByNumberAndHostidAndIdNot(String number,String hostid,String id) ;

    public Page<NumberPool> findAll(Specification<NumberPool> spec, Pageable page);

    @Query(value = "SELECT np.* FROM uk_number_pool np LEFT JOIN uk_number_pool_extention_rela npe ON np.id = npe.numberpoolid where npe.extentionid = ?1 order by createtime desc"
            ,nativeQuery = true)
    List<NumberPool> findByExtentionid(String extentionid);

}
