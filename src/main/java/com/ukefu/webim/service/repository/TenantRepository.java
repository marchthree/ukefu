package com.ukefu.webim.service.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ukefu.webim.web.model.Tenant;


public abstract interface TenantRepository extends JpaRepository<Tenant, String> {
	
	public abstract Tenant findById(String id);
	
	public abstract List<Tenant> findByOrgid(String orgid);

	public abstract Tenant findByOrgidAndTenantname(String orgid, String tenantname);

	public Page<Tenant> findAll(Specification<Tenant> spec, Pageable pageable);  //分页按条件查询 
	
	public List<Tenant> findAll(Specification<Tenant> spec);  //按条件查询

	public List<Tenant> findByDatastatus(boolean datastatus);
	
	public abstract Tenant findByTenantcode(String tenantcode);

	public abstract Tenant findByTenantcodeAndDatastatus(String tenantcode,boolean datastatus);

	public abstract Page<Tenant> findByTenantcodeIn(List<String> tenantcodes, Pageable pageable);
}
