package com.ukefu.webim.service.repository;

import com.ukefu.webim.web.model.SalesPatterMedia;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SalesPatterMediaRepository extends JpaRepository<SalesPatterMedia, String> {

	public SalesPatterMedia findByIdAndOrgi(String id, String orgi);

	public SalesPatterMedia findById(String id);

	public List<SalesPatterMedia> findBySalespatteridAndOrgi(String salespatterid, String orgi);

	public Page<SalesPatterMedia> findBySalespatteridAndOrgi(String salespatterid, String orgi, Pageable paramPageable);

	public List<SalesPatterMedia> findByOrgi(String orgi);
}
