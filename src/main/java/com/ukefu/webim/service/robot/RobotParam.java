package com.ukefu.webim.service.robot;

public class RobotParam implements java.io.Serializable{

	private static final long serialVersionUID = -1635819709234669477L;

	private String userid;//话术业务编号,标识某类业务,假如没设置,默认是0.通道变量:sip_h_userid 假如没设置,默认:对于拨入是被叫号码.对于拨出是主叫号码.
	private String telno;// 手机号,比如:13606060253
	private String text;//用户说话的语音,经过识别引擎生成的文本,比如"你好",假如语音播放完成,用户超时没说话 text="timeout"
	private String cdrid;//通话记录fs的通话的uuid,比如:xxxx1000014974
	private String taskid;//呼叫任务id,假如没设置,默认是0.通道变量:sip_h_taskid.
	private String wavfile;// 用户说话的语音保存的录音文件名称
	private String param1;//扩展参数1,比如:场景的步骤,="-99"表示用户AI对话结束,用户挂机了
	private String param2;//扩展参数2,param2="E" 表示语音播放完成,此时假如用户说的话没理解出来出来,需要播放一个类似没听清的语音.

	private String custom;
	private String playtime;

	private String action;

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTelno() {
		return telno;
	}

	public void setTelno(String telno) {
		this.telno = telno;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCdrid() {
		return cdrid;
	}

	public void setCdrid(String cdrid) {
		this.cdrid = cdrid;
	}

	public String getTaskid() {
		return taskid;
	}

	public void setTaskid(String taskid) {
		this.taskid = taskid;
	}

	public String getWavfile() {
		return wavfile;
	}

	public void setWavfile(String wavfile) {
		this.wavfile = wavfile;
	}

	public String getParam1() {
		return param1;
	}

	public void setParam1(String param1) {
		this.param1 = param1;
	}

	public String getParam2() {
		return param2;
	}

	public void setParam2(String param2) {
		this.param2 = param2;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(String custom) {
		this.custom = custom;
	}

	public String getPlaytime() {
		return playtime;
	}

	public void setPlaytime(String playtime) {
		this.playtime = playtime;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return "RobotParam{" +
				"userid='" + userid + '\'' +
				", telno='" + telno + '\'' +
				", text='" + text + '\'' +
				", cdrid='" + cdrid + '\'' +
				", taskid='" + taskid + '\'' +
				", wavfile='" + wavfile + '\'' +
				", param1='" + param1 + '\'' +
				", param2='" + param2 + '\'' +
				", custom='" + custom + '\'' +
				", playtime='" + playtime + '\'' +
				", action='" + action + '\'' +
				'}';
	}
}
